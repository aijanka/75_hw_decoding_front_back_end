import axios from 'axios';
import {ADD_DECODED_INFO, ADD_ENCODED_INFO} from "./actionTypes";

export const addEncodedInfo = (encoded, decoded) => ({type: ADD_ENCODED_INFO, encoded, decoded});

export const encode = object => {
    return (dispatch) => {
        // JSON.stringify(object)
        return axios.post('/encode', object).then(response => {
            dispatch(addEncodedInfo(response.data.encoded, object.message));
            console.log(response);

        });
    }
}


export const addDecodedInfo = (encoded, decoded) => ({type: ADD_DECODED_INFO, encoded, decoded});

export const decode = object => {
    return (dispatch) => {
        // JSON.stringify(object)
        return axios.post('/decode', object).then(response => {
            dispatch(addDecodedInfo(object.message, response.data.decoded));
            console.log(response);
        });
    }
};