import {ADD_DECODED_INFO, ADD_ENCODED_INFO} from "./actionTypes";

const initialState = {
    decoded: '',
    encoded: '',
    password: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_ENCODED_INFO:
            return {...state, encoded: action.encoded};
        case ADD_DECODED_INFO: 
            return {...state, decoded: action.decoded};
        default:
            return state;
    }
};

export default reducer;