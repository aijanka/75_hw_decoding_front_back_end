import React, {Component} from 'react';
import './App.css';
import {decode, encode} from "./store/actions";
import {connect} from "react-redux";

class App extends Component {
    state = {
        decoded: '',
        encoded: '',
        password: ''
    };

    saveChange = (event) => {
        this.setState({[event.target.name]: event.target.value});
    };

    encode = (event) => {
        event.preventDefault();
        this.props.encode({
            password: this.state.password,
            message: this.state.decoded
        })
            .then(() => {
                console.log(this.props.encoded);
                const encoded = this.props.encoded;
                this.setState({encoded});
            })
    };

    decode = (event) => {
        event.preventDefault();
        this.props.decode({
            password: this.state.password,
            message: this.state.encoded
        })
            .then(() => {
                console.log(this.props.decoded);
                const decoded = this.props.decoded;
                this.setState({decoded});
            })
    };

    render() {
        return (
            <div className="App">
                <div className="Decoded">
                    <label htmlFor="">Decoded message</label>
                    <input type="text" name='decoded'
                           onChange={this.saveChange}
                           value={this.state.decoded}
                    />
                </div>


                <label htmlFor="">password</label>
                <input type="text" name='password'
                       onChange={this.saveChange}
                       value={this.state.password}
                />

                <button onClick={this.encode}><i className="fas fa-angle-down"/></button>
                <button onClick={this.decode}><i className="fas fa-angle-up"/></button>

                <div className="Encoded">
                    <label htmlFor="">Encoded message</label>
                    <input type="text" name="encoded"
                           onChange={this.saveChange}
                           value={this.state.encoded}
                    />
                </div>

            </div>
        );
    }
}

const mapStateToProps = state => ({
    encoded: state.encoded,
    decoded: state.decoded
});

const mapDispatchToProps = dispatch => ({
    encode: (object) => dispatch(encode(object)),
    decode: (object) => dispatch(decode(object))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
