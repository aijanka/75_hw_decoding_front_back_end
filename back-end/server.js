const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const fs = require('fs');
const cors = require('cors');

const app = express();
const port = 8000;
let password = '';

fs.readFile('./dbPassword.json', (err, result) => {
    if (!err) {password = JSON.parse(result).password;}
});

app.use(cors());
app.use(express.json());

app.post('/encode', (req, res) => {
    if(req.body.password === password) {
        const encoded = Vigenere.Cipher('password').crypt(req.body.message);
        res.send({"encoded": encoded});
    }
});

app.post('/decode', (req, res) => {
    if(req.body.password === password) {
        const decoded = Vigenere.Decipher('password').crypt(req.body.message);
        res.send({"decoded": decoded});
    }
});

app.listen(port);